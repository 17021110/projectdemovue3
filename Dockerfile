FROM node:lts-alpine as builder
WORKDIR /src
COPY package*.json yarn.lock ./

RUN yarn install

COPY . .

RUN npm run build:dev

FROM nginx:alpine

COPY ./utilities/nginx.conf /etc/nginx/nginx.conf

COPY --from=builder /src/dist /app

EXPOSE 80

import { createApp } from "vue";

import App from "./index.vue";
import { store } from "./store";
import { Tree, Select, Modal, TreeSelect, Upload, Empty, DatePicker, TimePicker, Spin ,InputNumber} from "ant-design-vue";
import router from "./router";
// fontawesome
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);
import { fab } from "@fortawesome/free-brands-svg-icons";
library.add(fab);
import { far } from "@fortawesome/free-regular-svg-icons";
library.add(far);
import { dom } from "@fortawesome/fontawesome-svg-core";
dom.watch();

// library
import { i18n } from "./languages/i18n";
import { useI18n } from "vue-i18n";

import vClickOutside from "click-outside-vue3";

import "./assets/styles/tailwind.css";

// toast
import Vue3Toastify, { type ToastContainerOptions } from "vue3-toastify";

// style
import "./style.scss";
import "ant-design-vue/dist/antd.less";
import "vue3-toastify/dist/index.css";
import "nprogress/nprogress.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "bootstrap/dist/js/bootstrap.js";
import { getUser } from "./utils";

import  Analytics from "@/use/useFirebase";

// create app
const app = createApp(App, {
  setup() {
    const { t } = useI18n();
    return { t };
  },
});

// Register a global custom directive called `v-focus`
app.directive("focus", {
  // When the bound element is mounted into the DOM...
  mounted(el, binding) {
    // Focus the element
    if (binding.value) el.focus();
  },
});

app.directive("hasPermission", {
  // When the bound element is mounted into the DOM...
  mounted(el, binding) {
    if (binding.value) {
      let permissions = getUser()?.data?.permissions;
      let value = binding.value.split(",");
      let flag = true;
      for (let v of value) {
        if (!permissions.includes(v)) {
          flag = false;
        }
      }
      if (!flag) {
        if (!el.parentNode) {
          el.style.display = "none";
        } else {
          el.parentNode.removeChild(el);
        }
      }
    }
  },
});

// use third party lib
app.component("font-awesome-icon", FontAwesomeIcon);
app.use(Vue3Toastify, {
  autoClose: 3000,
} as ToastContainerOptions);
app.use(router);
app.use(i18n);
app.use(store);
app.use(vClickOutside);
app.use(Tree);
app.use(Select);
app.use(Modal);
app.use(TreeSelect);
app.use(DatePicker);
app.use(TimePicker);
app.use(Upload);
app.use(Spin);
app.use(InputNumber);
app.use(Empty);
app.use(Analytics);
// mount app
app.mount("#app");

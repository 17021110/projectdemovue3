export default {
  user: {
    SEARCH: {
      url: "/user",
      method: "GET",
    },
    CREATE: {
      url: "/user",
      method: "POST",
    },
    UPDATE: {
      url: "/user",
      method: "PUT",
    },
    DELETE: {
      url: "/user",
      method: "DELETE",
    },
    GET_USER: {
      url: "/user/get-user-info",
      method: "GET",
    },
    GET_USER_BY_ID: {
      url: "/user",
      method: "GET",
    },
    GET_ALL: {
      url: "/user/get-all",
      method: "GET",
    },
    GET_USER_IN_ASSESSMENT: {
      url: "/user/get-by-assessment-id",
      method: "GET",
    },
    EXPORT: {
      url: "user-service/api/v1/user/export-excel",
      method: "GET",
    },
    EXPORT_TEMPLATE: {
      url: "user-service/api/v1/user/export-template-excel",
      method: "GET",
    },
    IMPORT_USER: {
      url: "/user/import-excel",
      method: "POST",
    },
    DETAIL_DIAGRAM: {
      url: "/user/detail-diagram",
      method: "GET",
    },
  },
  auth: {
    LOGIN: {
      url: "/login",
      method: "POST",
    },
    LOGOUT: {
      url: "/logout",
      method: "DELETE",
    },
    CHANGE_PASSWORD: {
      url: "/change-passwork",
      method: "PATCH",
    },
    FORGOT_PASSWORD: {
      url: "/forgot-password",
      method: "PATCH",
    },
    RESET_PASSWORD: {
      url: "/reset-password",
      method: "GET",
    },
    CONSULTION: {
      url: "/consultant-request",
      method: "POST",
    },
  },
  role: {
    GET_ALL: {
      url: "/role/get-all",
      method: "GET",
    },
    SEARCH: {
      url: "/role",
      method: "GET",
    },
    CREATE: {
      url: "/role",
      method: "POST",
    },
    UPDATE: {
      url: "/role",
      method: "PUT",
    },
    DELETE: {
      url: "/role",
      method: "DELETE",
    },
  },
  role_menu: {
    GET_BY_ROLE: {
      url: "/demo/role-menu",
      method: "GET",
    },
  },
  menu: {
    SEARCH: {
      url: "/demo/menu",
      method: "GET",
    },
  },
  department: {
    GET_BY_ALL: {
      url: "/demo/department/get-all",
      method: "GET",
    },
    SEARCH: {
      url: "/demo/department",
      method: "GET",
    },
    CREATE: {
      url: "/demo/department",
      method: "POST",
    },
    UPDATE: {
      url: "/demo/department",
      method: "PUT",
    },
    DELETE: {
      url: "/demo/department",
      method: "DELETE",
    },
    DIAGRAM: {
      url: "/diagram",
      method: "GET",
    },
    DETAIL: {
      url: "/demo/department/detail",
      method: "GET",
    },
    GET_ALL_BY_COMPANY_ID: {
      url: "/demo/department/getByCompanyId",
      method: "GET",
    },
    GET_DEPARTMENT_IN_ASSESSMENT: {
      url: "/demo/department/get-by-assessment-id",
      method: "GET",
    },
  },
  position: {
    SEARCH: {
      url: "/position-title",
      method: "GET",
    },
    CREATE: {
      url: "/position-title",
      method: "POST",
    },
    UPDATE: {
      url: "/position-title",
      method: "PUT",
    },
    DELETE: {
      url: "/position-title",
      method: "DELETE",
    },
    RECURSIVE: {
      url: "/demo/position-title/recursive-list-all",
      method: "GET",
    },
    GET_ALL_BY_DEPARTMENT_ID: {
      url: "/demo/position-title/get-by-departmentId",
      method: "GET",
    },
    GET_DETAIL: {
      url: "/position-title",
      method: "GET",
    },
    GET_TREE: {
      url: "/demo/position-title/get-all-form-tree",
      method: "GET",
    },
    GET_ALL_BY_COMPANY_ID: {
      url: "/demo/capacity_frameworks/position_no_cap",
      method: "GET",
    },
  },
  upload: {
    UPLOAD_IMAGE: {
      url: "demo/file/upload",
      method: "POST",
    },
  },
};

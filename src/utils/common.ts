import { toast } from "vue3-toastify";
import format from "date-fns/format";
import parse from "date-fns/parse";
import isBefore from "date-fns/isBefore";

const checkResponse = (
  response,
  success = (response) => {},
  fail = (response) => {
    toast.error(response.message || "Lỗi");
  },
) => {
  if (response.code === "200") {
    success(response.data);
  } else {
    fail(response.data);
  }
};

const formatDate = (date, formatType) => {
  return format(new Date(date), formatType || "dd/MM/yyyy HH:mm:ss");
};

const compareDate = (date1, date2) => {
  const temp1 = new Date(date1);
  const temp2 = new Date(date2);
  return isBefore(temp1, temp2);
};

const convertDate = (date, formatType) => {
  const parsedDate = parse(date, "yyyy-MM-dd", new Date());
  const formattedDate = format(parsedDate, formatType || "yyyy-MM-dd HH:mm:ss");
  return formattedDate;
};

const formatMoney = (format, price) => {
  let result =new Intl.NumberFormat(format).format(price);
  return result;
};


export { checkResponse, formatDate, convertDate, compareDate,formatMoney };

import { RouteRecordRaw } from "vue-router";
import LayoutDefaut from "@/components/layout/defaultLayout/index.vue";
import LayoutDetail from "@/components/layout/layoutDetail/index.vue";

export default (): RouteRecordRaw[] => {
  return [
    {
      path: "/demo",
      name: "demo",
      component: () => import("./login/index.vue"),
      meta: {
        title: "login",
        publicRoute: true,
      },
    },
  ];
};

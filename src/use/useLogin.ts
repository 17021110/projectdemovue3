import useVuelidate from "@vuelidate/core";
import { useRoute, useRouter } from "vue-router";
import { toast } from "vue3-toastify";
import { setToken } from "@/utils/index";
import { useStore } from "vuex";
import ConstantAPI from "@/services/ConstantAPI";
import { DataService } from "@/services/request";
import { ref } from "vue";

export const useLogin = (rules, formData) => {
  const v$ = useVuelidate(rules, formData);
  const route = useRoute();
  const store = useStore();
  const router = useRouter();
  const loading = ref<boolean>(false);
  const submit = async () => {
    v$.value.$touch();
    if (v$.value.$invalid) {
      return;
    }
    try {
      loading.value = true;
      formData.value.username = formData.value.username.trim();
      const response = await DataService.callApi(ConstantAPI.auth.LOGIN, formData.value);
      if (response) {
        await setToken(JSON.stringify(response));
        await redirectPage();
        toast.success("Đăng nhập thành công!");
        loading.value = false;
      } else {
        loading.value = false;
        toast.error("đăng nhập lỗi");
      }
    } catch (error: any) {
      loading.value = false;
      toast.error(error?.error_description);
    }
  };
  const redirectPage = (): void => {
    const defaultUrl = `/`;

    let url = route.query.redirect ? "" + route.query.redirect : defaultUrl;
    router.push({ path: url });
  };

  return { v$, submit, loading };
};

import { RootState } from "./store.type";
import { createStore, ModuleTree, Module } from "vuex";
import role from "./modules/role";
import menu from "./modules/menu";

import profile from "./modules/profile";

const modules: ModuleTree<RootState> = {
  role,
  menu,
  profile,
};

const root: Module<RootState, RootState> = {
  modules,
};

// create Store
export const store = createStore<RootState>({
  ...root,
});

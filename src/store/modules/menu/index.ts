import ConstantAPI from "@/services/ConstantAPI";
import { DataService } from "@/services/request";

export interface MenuState {
  menus: null | any;
  loading: boolean;
  error: null | any;
}
export default {
  namespaced: true as true,
  state: {
    menus: null,
    loading: true,
    error: null,
  } as MenuState,
  getters: {
    menus: (state) => state.menus,
    loading: (state) => state.loading,
    error: (state) => state.error,
  },
  mutations: {
    FETCH_MENUS_BEGIN(state) {
      state.loading = true;
    },
    SET_MENUS(state, payload) {
      state.loading = false;
      state.menus = payload;
    },
    FETCH_MENUS_ERR(state, err) {
      state.loading = false;
      state.error = err;
    },
  },
  actions: {
    async fetchMenus({ commit }) {
      try {
        commit("FETCH_MENUS_BEGIN");
        const response = await DataService.callApi(ConstantAPI.menu.SEARCH);
        return commit("SET_MENUS", response.data);
      } catch (error) {
        commit("FETCH_MENUS_ERR", error);
      }
    },
  },
};

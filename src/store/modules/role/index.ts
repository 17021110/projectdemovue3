import ConstantAPI from "@/services/ConstantAPI";
import { DataService } from "@/services/request";

export interface RoleState {
  roles: null | any;
  loading: boolean;
  error: null | any;
}
export default {
  namespaced: true as true,
  state: {
    roles: null,
    loading: true,
    error: null,
  } as RoleState,
  getters: {
    roles: (state) => state.roles,
    loading: (state) => state.loading,
    error: (state) => state.error,
  },
  mutations: {
    FETCH_ROLES_BEGIN(state) {
      state.loading = true;
    },
    SET_ROLES(state, payload) {
      state.loading = false;
      state.roles = payload;
    },
    FETCH_ROLES_ERR(state, err) {
      state.loading = false;
      state.error = err;
    },
  },
  actions: {
    async fetchRoles({ commit }, params) {
      try {
        commit("FETCH_ROLES_BEGIN");
        const response = await DataService.callApi(ConstantAPI.role.SEARCH, null, params);
        return commit("SET_ROLES", response.data);
      } catch (error) {
        commit("FETCH_ROLES_ERR", error);
      }
    },
    async createRole({ dispatch }, payload) {
      try {
        const response = await DataService.callApi(ConstantAPI.role.CREATE, payload.data);
        await dispatch("fetchRoles", payload.params);
        return response;
      } catch (error) {
        console.log(error);
      }
    },
    async updateRole({ dispatch }, payload) {
      try {
        const response = await DataService.callApi(ConstantAPI.role.UPDATE, payload.data, null);
        await dispatch("fetchRoles", payload.params);
        return response;
      } catch (error) {
        console.log(error);
      }
    },
    async deleteRole({ dispatch }, payload) {
      try {
        const response = await DataService.delete(ConstantAPI.role.DELETE.url + "/" + payload.ids, null, null);
        await dispatch("fetchRoles", payload.params);
        return response;
      } catch (error) {
        console.log(error);
      }
    },
  },
};

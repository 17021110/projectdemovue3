import ConstantAPI from "@/services/ConstantAPI";
import { DataService } from "@/services/request";

export interface analyseCapacity {
  dataUser: null | any;
  loading: boolean;
  error: null | any;
}
export default {
  namespaced: true as true,
  state: {
    dataUser: null,
    loading: true,
    error: null,
  } as analyseCapacity,
  getters: {
    dataUser: (state) => state.dataUser,
    loading: (state) => state.loading,
    error: (state) => state.error,
  },
  mutations: {
    FETCH_DATA_BEGIN(state) {
      state.loading = true;
    },
    FETCH_DATA_END(state) {
      state.loading = false;
    },
    SET_DATA(state, payload) {
      state.loading = false;
      state.dataUser = payload;
    },
    FETCH_DATA_ERR(state, err) {
      state.loading = false;
      state.error = err;
    },
  },
  actions: {
    async fetchData({ commit }, params) {
      try {
        commit("FETCH_DATA_BEGIN");
        const response = await DataService.get(`${ConstantAPI.user.GET_USER_BY_ID.url}/${params?.id}`, null, null);
        commit("SET_DATA", response.data);
        return response.data;
      } catch (error) {
        commit("FETCH_DATA_ERR", error);
      }
    },

    async updateProfile({ commit, dispatch }, payload) {
      try {
        const response = await DataService.put(`${ConstantAPI.user.UPDATE.url}`, payload, null);
        return response;
      } catch (error) {
        commit("FETCH_DATA_ERR", error);
      }
    },
  },
};

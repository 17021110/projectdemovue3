export const vi = {
  detail: "Detail",
  validate: {
    requiredInput: "Xin vui lòng hoàn thiện %s",
    requiredSelect: "Xin vui lòng chọn %s",
    maxString: "%s không được phép nhập quá %n kí tự",
    minMaxString: "%s có %n ký tự trở lên và %n ký tự trở xuống.",
    betweenValue: "%s chỉ được nhập số từ %n đến %n",
    minValue: "%s không được là số âm",
    smallValue: "%s phải lớn hơn hoặc bằng %n",
    maxValue: "%s không được lớn hơn 100",
    $each: "Hoàn thiện các thành phần",
    validateEmail: "Email sai định dạng",
    alphaNum: "%s không được nhập kí tự đặc biệt, khoảng trắng, tiếng việt có dấu",
    space: "%s không được phép nhập khoảng trắng",
    minDate: "%s phải lớn hơn ngày hiện tại",
    confirmPasswordNotMatch: "Mật khẩu mới và mật khẩu xác nhận chưa giống nhau",
    notSameAsoldPassword: "Mật khẩu mới phải khác mật khẩu cũ",
    numeric: "%s chỉ được nhập số",
    integer: "%s Chỉ được nhập số nguyên dương",
    telephone:"%s sai định dạng",
    required1orAll:"Bạn chỉ cần nhập 1 trong 2 trường Email hoặc Số điện thoại"
  }
};
